# 90 Seconds Assignment

In this project implemented for showing list of videos from one of 90 Seconds [playlist](https://www.youtube.com/watch?v=HQKkHydv7u0&list=PLyRfJw1I1N7HOCVMCJ3xpvKmAfZBv684-) .When user select a video from the list, it will play that video in another screen.(created a screen recording in 90s-assignment>video>90s.mp4)

## Build With

 1. jdk8
 2. android studio 3.5.3
 3. gradle-android-plugin 3.5.3 (need to sync before build the project)
 4. kotlin-version 1.3.50

## Tech stack currently using

 1. Clean architecture with MVVM (we can easily migrate to other services such us Viemo)
 2. 100% kotlin project
 3. Dagger for dependency for injection
 4. concurrency with kotlin coroutine
 5. architecture components (life cycle and view model)
 6. AndroidX
 7. Debugging with Flipper from Facebook
 8. image loading with Glide

## Need to be improved

 1. Although ui is still responsive for tablet. it will be better if we make mater/detail pane for bigger screen devices.
 2. still need to cover unit test
 3. need feature of pagination for getting video list.
