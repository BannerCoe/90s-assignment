package com.amlwin.a90s.di

import android.content.Context
import com.amlwin.a90s.MainApplication
import com.amlwin.a90s.data.config.PreferenceStorage
import com.amlwin.a90s.data.config.SharedPreferenceStorage
import com.amlwin.a90s.data.repository.VideoListRemoteDataSource
import com.amlwin.a90s.data.repository.VideoListRemoteDataSourceImpl
import com.amlwin.a90s.data.repository.VideoListRepository
import com.amlwin.a90s.data.repository.VideoListRepositoryImpl
import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.http.HttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.client.util.ExponentialBackOff
import com.google.api.services.youtube.YouTubeScopes
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ShareModule {


    @Provides
    fun provideContext(application: MainApplication): Context {
        return application.applicationContext
    }

    @Singleton
    @Provides
    fun providesPreferenceStorage(context: Context): PreferenceStorage =
        SharedPreferenceStorage(context)

    @Singleton
    @Provides
    fun provideYouTubeService(
        httpTransport: HttpTransport,
        jsonFactory: JsonFactory,
        googleAccountCredential: GoogleAccountCredential
    ): com.google.api.services.youtube.YouTube {
        return com.google.api.services.youtube.YouTube.Builder(
            httpTransport,
            jsonFactory,
            googleAccountCredential
        ).setApplicationName("90s").build()
    }

    @Singleton
    @Provides
    fun provideGoogleAccountCredentail(context: Context): GoogleAccountCredential {
        return GoogleAccountCredential.usingOAuth2(
            context,
            arrayListOf(YouTubeScopes.YOUTUBE_READONLY)
        )
            .setBackOff(ExponentialBackOff())
    }

    @Singleton
    @Provides
    fun provideHttpTransport(): HttpTransport {
        return AndroidHttp.newCompatibleTransport()
    }

    @Singleton
    @Provides
    fun provideYouTubeGsonFactory(): JsonFactory {
        return JacksonFactory.getDefaultInstance()
    }

    @Singleton
    @Provides
    fun provideVideoListRepository(remoteDataSource: VideoListRemoteDataSource): VideoListRepository {
        return VideoListRepositoryImpl(remoteDataSource)
    }

    @Singleton
    @Provides
    fun provideVideoRemoteDataSource(service: com.google.api.services.youtube.YouTube): VideoListRemoteDataSource =
        VideoListRemoteDataSourceImpl(service)

}