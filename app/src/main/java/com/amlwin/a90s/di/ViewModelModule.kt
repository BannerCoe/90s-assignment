package com.amlwin.a90s.di

import androidx.lifecycle.ViewModelProvider
import com.amlwin.a90s.vm.MainViewModelFactory
import dagger.Binds
import dagger.Module

@Module
@Suppress("UNUSED")
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: MainViewModelFactory):
            ViewModelProvider.Factory
}