package com.amlwin.a90s.di

import com.amlwin.a90s.MainActivity
import com.amlwin.a90s.detail.DetailActivity
import com.amlwin.a90s.detail.DetailModule
import com.amlwin.a90s.list.PlayListModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [PlayListModule::class]
    )
    abstract fun provideMain(): MainActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [DetailModule::class])
    abstract fun provideDetail(): DetailActivity
}