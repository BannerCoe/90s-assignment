package com.amlwin.a90s.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amlwin.a90s.R
import dagger.android.support.DaggerFragment

class DetailFragment:DaggerFragment() {
    private lateinit var rootView:View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
         super.onCreateView(inflater, container, savedInstanceState)
        rootView = inflater.inflate(R.layout.fragment_detail,container,false)
        return rootView
    }
}