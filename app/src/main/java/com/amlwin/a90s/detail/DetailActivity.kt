package com.amlwin.a90s.detail

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.amlwin.a90s.BuildConfig
import com.amlwin.a90s.R
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerFragment
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : YouTubeBaseActivity(), YouTubePlayer.OnInitializedListener {
    private val RECOVERY_DIALOG_REQUEST = 105

    private var videoId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val youTubePlayerFragment = fg_yt_player as YouTubePlayerFragment
        youTubePlayerFragment.initialize(BuildConfig.devKey, this)

        if (intent != null) {
            videoId = intent.getStringExtra(key_video_id) ?: ""
        }
    }

    override fun onInitializationSuccess(
        provider: YouTubePlayer.Provider?,
        player: YouTubePlayer?,
        wasRestored: Boolean
    ) {
        player?.cueVideo(videoId)
    }

    override fun onInitializationFailure(
        provider: YouTubePlayer.Provider?,
        errorReason: YouTubeInitializationResult?
    ) {
        if (errorReason!!.isUserRecoverableError) {
            errorReason.getErrorDialog(
                this,
                RECOVERY_DIALOG_REQUEST
            ).show()
        } else {
            val errorMessage =
                String.format("Error player", errorReason.toString())
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            getYouTubePlayerProvider().initialize(BuildConfig.devKey, this)
        }
    }

    private fun getYouTubePlayerProvider(): YouTubePlayer.Provider {
        return (fg_yt_player as YouTubePlayerFragment)
    }

    companion object {
        const val key_video_id = "key_video_id"
    }
}
