package com.amlwin.a90s.list

import androidx.lifecycle.ViewModel
import com.amlwin.a90s.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class PlayListModule {

    @ContributesAndroidInjector
    internal abstract fun contributePlayListFragment(): PlayListFragment

    @Binds
    @IntoMap
    @ViewModelKey(PlayListViewModel::class)
    abstract fun bindPlayListViewModel(viewModel: PlayListViewModel): ViewModel

}