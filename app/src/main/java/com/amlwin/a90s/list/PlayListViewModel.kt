package com.amlwin.a90s.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.amlwin.a90s.domain.LoadVideoUseCase
import com.amlwin.a90s.domain.SaveUserNameUseCase
import com.amlwin.a90s.utils.Result
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException
import com.google.api.services.youtube.model.PlaylistItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class PlayListViewModel @Inject constructor(
    private val saveUserNameUseCase: SaveUserNameUseCase,
    private val loadVideoUseCase: LoadVideoUseCase
) : ViewModel() {

    private val _loading = MutableLiveData<Boolean>(false)
    val loading get() = _loading

    private val _userRecoverAuthException = MutableLiveData<Result.Error>()
    val userRecoverAuthException get() = _userRecoverAuthException

    private val _playList = MutableLiveData<List<PlaylistItem>>()
    val playList get() = _playList

    fun saveAccountName(accName: String) {
        saveUserNameUseCase.execute(accName)
    }

    fun loadVideo() {
        viewModelScope.launch(Dispatchers.IO) {
            _loading.postValue(true)
            Timber.d("start vm")
            val result = loadVideoUseCase.execute()
            when (result) {
                is Result.Error -> handleError(result)
                is Result.Success -> {
                    _loading.postValue(false)
                    Timber.d("banner: success ${result.data}")
                    _playList.postValue(result.data.items)
                }
            }
        }

    }

    private fun handleError(result: Result.Error) {
        _loading.postValue(false)
        if (result.exception is UserRecoverableAuthIOException) {
            _userRecoverAuthException.postValue(result)
        }
    }
}