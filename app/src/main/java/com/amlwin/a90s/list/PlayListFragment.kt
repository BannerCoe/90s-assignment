package com.amlwin.a90s.list

import android.Manifest
import android.accounts.AccountManager
import android.app.Activity.RESULT_OK
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.amlwin.a90s.R
import com.amlwin.a90s.data.config.PreferenceStorage
import com.amlwin.a90s.detail.DetailActivity
import com.amlwin.a90s.utils.Result
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.material.snackbar.Snackbar
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_playlist.*
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import timber.log.Timber
import javax.inject.Inject


private const val REQUEST_PERMMSION_ACCOUNT = 101

class PlayListFragment : DaggerFragment(), EasyPermissions.PermissionCallbacks {

    private val REQUEST_GOOGLE_PLAY_SERVICES = 100
    private val REQUEST_ACCOUNT_PICKER = 102
    private val REQUEST_PERMISSION_GET_ACCOUNTS = 103
    private val REQUEST_AUTHORIZATION = 104

    private lateinit var rootView: View

    lateinit var vm: PlayListViewModel
    lateinit var adapter: PlayListAdapter

    @Inject
    lateinit var preferenceStorage: PreferenceStorage

    @Inject
    lateinit var vmFactory: ViewModelProvider.Factory

    @Inject
    lateinit var googleAccountCredential: GoogleAccountCredential

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        rootView = inflater.inflate(R.layout.fragment_playlist, container, false)
        Timber.d("banner: start")
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm = ViewModelProviders.of(this, vmFactory)[PlayListViewModel::class.java]


        initRecycler()
        init()

        vm.userRecoverAuthException.observe(viewLifecycleOwner, Observer {
            requestAuth(it)
        })

        vm.playList.observe(viewLifecycleOwner, Observer {
            adapter.setPlayList(it)
        })

        vm.loading.observe(viewLifecycleOwner, Observer {
            pb_loading.visibility = if (it) View.VISIBLE else View.GONE
        })

    }

    private fun initRecycler() {
        adapter = PlayListAdapter()
        adapter.setOnRowClickListener {
            Timber.d("banner: click at $it")
            goToDetailPage(it.snippet.resourceId.videoId)
        }
        rv_play_list.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        rv_play_list.adapter = adapter
    }

    private fun goToDetailPage(videoId: String) {
        /*requireActivity()?.supportFragmentManager.beginTransaction()
            .replace(R.id.fl_container, DetailFragment())
            .addToBackStack(this.tag)
            .commit()*/
        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra(DetailActivity.key_video_id, videoId)
        startActivity(intent)
    }

    private fun requestAuth(it: Result.Error) {
        startActivityForResult(
            (it.exception as UserRecoverableAuthIOException).intent,
            REQUEST_AUTHORIZATION
        )
    }

    private fun init() {
        if (!isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices()
        } else if (googleAccountCredential.selectedAccountName == null) {
            Timber.d("banner: i'm here")
            chooseAccount()
        } else if (!isDeviceOnline()) {
            Snackbar.make(fl_root_view, "Please make device online", Snackbar.LENGTH_LONG).show()
        } else {
            vm.loadVideo()
        }


    }

    @AfterPermissionGranted(REQUEST_PERMMSION_ACCOUNT)
    private fun chooseAccount() {
        if (EasyPermissions.hasPermissions(requireContext(), Manifest.permission.GET_ACCOUNTS)) {
            val accountName = preferenceStorage.accountName
            Timber.d("banner: get account name is $accountName")
            if (accountName != null) {
                googleAccountCredential.selectedAccountName = accountName
                vm.loadVideo()
            } else {
                startActivityForResult(
                    googleAccountCredential.newChooseAccountIntent(),
                    REQUEST_ACCOUNT_PICKER
                )
            }
        } else {
            EasyPermissions.requestPermissions(
                this,
                "This app needs to access your Google account (via Contacts).",
                REQUEST_PERMISSION_GET_ACCOUNTS,
                Manifest.permission.GET_ACCOUNTS
            )
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_ACCOUNT_PICKER -> {
                if (resultCode == RESULT_OK) {
                    saveAccountName(data)
                    googleAccountCredential.selectedAccountName = preferenceStorage.accountName
                    vm.loadVideo()
                }
            }
            REQUEST_AUTHORIZATION -> {
                if (resultCode == RESULT_OK) {
                    init()
                }

            }
            else -> {
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>?) {

    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>?) {
        chooseAccount()
    }

    private fun saveAccountName(data: Intent?) {
        val accountName: String? = data?.getStringExtra(AccountManager.KEY_ACCOUNT_NAME)
        Timber.d("banner: in save account name $accountName")
        if (accountName != null) {
            vm.saveAccountName(accountName)
        }
    }

    private fun acquireGooglePlayServices() {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val connectionStatusCode = apiAvailability.isGooglePlayServicesAvailable(context)
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode)
        }
    }

    private fun isGooglePlayServicesAvailable(): Boolean {
        val apiAvailability: GoogleApiAvailability = GoogleApiAvailability.getInstance()
        val connectionStatusCode: Int = apiAvailability.isGooglePlayServicesAvailable(context)
        return connectionStatusCode == ConnectionResult.SUCCESS
    }

    private fun showGooglePlayServicesAvailabilityErrorDialog(
        connectionStatusCode: Int
    ) {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val dialog: Dialog = apiAvailability.getErrorDialog(
            requireActivity(),
            connectionStatusCode,
            REQUEST_GOOGLE_PLAY_SERVICES
        )
        dialog.show()
    }

    private fun isDeviceOnline(): Boolean {
        val connMgr =
            requireContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        val networkInfo = connMgr!!.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }
}