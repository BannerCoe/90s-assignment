package com.amlwin.a90s.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.amlwin.a90s.R
import com.bumptech.glide.Glide
import com.google.api.services.youtube.model.PlaylistItem
import kotlinx.android.synthetic.main.row_play_list.view.*

class PlayListAdapter : RecyclerView.Adapter<PlayListAdapter.PlayListViewHolder>() {
    private val dataList = mutableListOf<PlaylistItem>()

    lateinit var clickListener: (PlaylistItem) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayListViewHolder {
        val rootView =
            LayoutInflater.from(parent.context).inflate(R.layout.row_play_list, parent, false)
        return PlayListViewHolder(rootView, clickListener)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: PlayListViewHolder, position: Int) {
        holder.bind(dataList[position])
    }

    fun setPlayList(list: List<PlaylistItem>) {
        dataList.addAll(list)
        notifyDataSetChanged()
    }

    fun setOnRowClickListener(f: (PlaylistItem) -> Unit) {
        this.clickListener = f
    }


    class PlayListViewHolder(itemView: View, clickListener: (PlaylistItem) -> Unit) :
        RecyclerView.ViewHolder(itemView) {
        lateinit var item: PlaylistItem
        init {
            itemView.cl_row_layout.setOnClickListener {
                clickListener(item)
            }
        }

        fun bind(extItem: PlaylistItem) {
            this.item = extItem
            itemView.tv_title.text = item.snippet.title
            Glide.with(itemView)
                .load(item.snippet.thumbnails.default.url)
                .into(itemView.iv_thumnail)
        }
    }

}