package com.amlwin.a90s

import android.os.Bundle
import com.amlwin.a90s.list.PlayListFragment
import dagger.android.support.DaggerAppCompatActivity

class MainActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction()
            .replace(R.id.fl_container, PlayListFragment())
            .commit()
    }

}
