package com.amlwin.a90s.data.repository

import com.amlwin.a90s.utils.Result
import com.google.api.services.youtube.model.PlaylistItemListResponse

interface VideoListRepository {
    suspend fun loadVideoPlayList(): Result<PlaylistItemListResponse>
}

class VideoListRepositoryImpl(
    val videoListRemoteDataSource: VideoListRemoteDataSource
) : VideoListRepository {
    override suspend fun loadVideoPlayList(): Result<PlaylistItemListResponse> {
        return videoListRemoteDataSource.loadPlayList()
    }

}