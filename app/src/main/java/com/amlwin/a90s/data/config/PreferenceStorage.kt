package com.amlwin.a90s.data.config

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import androidx.annotation.WorkerThread
import androidx.core.content.edit
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

interface PreferenceStorage {
    var accountName: String?
}

@Singleton
class SharedPreferenceStorage @Inject constructor(context: Context) :
    PreferenceStorage {
    private val prefs: Lazy<SharedPreferences> = lazy {
        // Lazy to prevent IO access to main thread.
        context.applicationContext.getSharedPreferences(
            "com.amlwin.a90s-preferences", MODE_PRIVATE
        )
    }
    override var accountName: String? by StringPreference(
        prefs,
        PREFS_ACCOUNT_NAME,
        null
    )

    companion object {
        const val PREFS_ACCOUNT_NAME = "account_name"
    }

}

class StringPreference(
    private val preferences: Lazy<SharedPreferences>,
    private val name: String,
    private val defaultValue: String?
) : ReadWriteProperty<Any, String?> {

    @WorkerThread
    override fun getValue(thisRef: Any, property: KProperty<*>): String? {
        return preferences.value.getString(name, defaultValue)
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: String?) {
        preferences.value.edit { putString(name, value) }
    }
}
