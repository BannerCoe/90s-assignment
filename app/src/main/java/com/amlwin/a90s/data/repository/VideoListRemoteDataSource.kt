package com.amlwin.a90s.data.repository

import com.amlwin.a90s.utils.Result
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException
import com.google.api.services.youtube.YouTube
import com.google.api.services.youtube.model.PlaylistItemListResponse
import timber.log.Timber
import javax.inject.Inject

interface VideoListRemoteDataSource {
    suspend fun loadPlayList(): Result<PlaylistItemListResponse>
}

class VideoListRemoteDataSourceImpl @Inject constructor(
    private val service: YouTube
) : VideoListRemoteDataSource {
    override suspend fun loadPlayList(): Result<PlaylistItemListResponse> {
        return try {
            Timber.d("start")
            /*val channelInfo: List<String> = ArrayList()
                val response = service.Channels().list("snippet,contentDetails,statistics")
                    .setForUsername("GoogleDevelopers")
                    .execute()

                val list = response.items*/

            val response = service.PlaylistItems().list("snippet,contentDetails").setMaxResults(25)
                .setPlaylistId("PLyRfJw1I1N7HOCVMCJ3xpvKmAfZBv684-").execute()
            Timber.d("banner: response $response.")
            Result.Success(response)
        } catch (e: UserRecoverableAuthIOException) {
            Result.Error(e)
        }

    }

}
