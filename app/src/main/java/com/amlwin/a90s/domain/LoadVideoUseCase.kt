package com.amlwin.a90s.domain

import com.amlwin.a90s.data.repository.VideoListRepository
import com.amlwin.a90s.utils.Result
import com.google.api.services.youtube.model.PlaylistItemListResponse
import javax.inject.Inject

class LoadVideoUseCase @Inject constructor(
    private val videoListRepository: VideoListRepository
) {

    suspend fun execute(): Result<PlaylistItemListResponse> {
        return videoListRepository.loadVideoPlayList()
    }
}