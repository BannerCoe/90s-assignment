package com.amlwin.a90s.domain

import com.amlwin.a90s.data.config.PreferenceStorage
import javax.inject.Inject

class SaveUserNameUseCase @Inject constructor(
    private val preferenceStorage: PreferenceStorage
) {
    fun execute(accName: String) {
        preferenceStorage.accountName = accName
    }

}